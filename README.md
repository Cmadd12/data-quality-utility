# Data Quality Utility

This utility utilizes a jupyter notebook to read in data and apply a quality check to ensure accurate creation of data. Aspects of the quality check include data length, delimiter, tokens being "approved".